# Quicklizard API Clients
---

This repository contains client libraries for Quicklizard's REST API, which can 
be used by you to work against our API.

#### Available Libraries - Official Support

* [Ruby](https://bitbucket.org/quicklizard/ql-api-client-ruby/)
* [Python](https://bitbucket.org/quicklizard/ql-api-client-python/)
* [Java](https://bitbucket.org/quicklizard/ql-api-client-java/)
* [PHP](https://bitbucket.org/quicklizard/ql-api-client-php/)
* [Node](https://bitbucket.org/quicklizard/ql-api-client-node/)
* [C#](https://bitbucket.org/quicklizard/ql-api-client-csharp/)

### Contributing

Contributions to existing client libraries or addition of clients in other languages are accepted.

* For existing clients - Please fork, fix, test and send PR
* For clients in additional languages, please add link to your repo below

#### Available Libraries - Unofficial Support